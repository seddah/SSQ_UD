# HOWTO produce UD_French-Sequoia (version 2.1)

## Clone the Conversion GRS (at TAG UD-2.1)
 * `git clone https://gitlab.inria.fr/grew/SSQ_UD.git --branch UD-2.1 --single-branch`
 * `cd SSQ_UD` 

## Download original Sequoia corpus (at TAG UD-2.1)
 * `wget https://gitlab.inria.fr/sequoia/deep-sequoia/raw/UD-2.1/trunk/sequoia.surf.conll`

## Apply the transformation
 * `grew transform -grs grs/ssq_to_ud/main.grs -i sequoia.surf.conll -o tmp.conllu`
 * `cat tmp.conllu | udapy -s ud.SetSpaceAfterFromText > fr_sequoia-ud-all.conllu`
 * `rm -f tmp.conllu`

## Split fr_sequoia-ud-all.conllu in 3 final files
 * `conll_tool split fr_sequoia-ud-all.conllu dev.ids fr_sequoia.ud.dev.conll`
 * `conll_tool split fr_sequoia-ud-all.conllu test.ids fr_sequoia.ud.test.conll`
 * `conll_tool split fr_sequoia-ud-all.conllu train.ids fr_sequoia.ud.train.conll`


