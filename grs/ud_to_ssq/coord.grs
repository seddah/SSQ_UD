% =============================================================================================
% Change coordinations from the UD format to the SSQ format
package coord_ud_ssq {

% Precondition: a conjunct is linked to another conjunct via a CONJ dependency and the coordination is marked with a conjunction or a comma, depending on the first conjunct.
% Action: the conjunction or the comma becomes the governor of the second conjunct.
  rule conj_cc_conj{
    pattern{
      conj_rel:CONJ1 -[conj]-> CONJ2;
      cc_rel: CONJ2 -[cc|punct]-> CC}
    without{ CC[cat=C,pos=CS] }
    without{ CC[cat=PONCT, form <> ";"|","|"-"|"/"|"--"] }
    without{ CONJ2 -[punct]-> CC; CONJ2 -[cc]-> * }
    without{ CONJ2 -[punct]-> CC; CONJ2 -> X; X << CC }
    commands{
      del_edge cc_rel;add_edge CONJ1 -[coord]-> CC;
      del_edge conj_rel; add_edge CC -[dep.coord]-> CONJ2}}

% Precondition: in a coordination, the first conjunct depends on a preposition and the second conjunct is a preposition.
% Action : the source of the COORD dependency becomes the preposition governor of the first conjunct.
% Ex : Europar 4
  rule prep_conj_cc_prep{
    pattern{
      PREP1 -[obj.p]-> OBJ;
      coord_rel: OBJ -[coord]-> PREP2; PREP2 -[obj.p]-> *}
    commands{del_edge coord_rel; add_edge PREP1 -[coord]-> PREP2}}

% Precondition: in a coordination, the first conjunct depends on a conjunction of subordination and the second conjunct is a conjunction of subordination.
% Action : the source of the COORD dependency becomes the conjunction governor of the first conjunct.
  rule cs_conj_cc_cs{
    pattern{
      CS1 -[obj.cpl]-> V;
      coord_rel: V -[coord]-> CS2; CS2 -[obj.cpl]-> *}
    commands{del_edge coord_rel; add_edge CS1 -[coord]-> CS2}}

% Precondition: in a coordination, the first conjunct is a subject predicative complement of a copula and the second conjunct is a verb.
% Action : the source of the COORD dependency becomes the governor of the first conjunct.
% Ex : annodis 19
  rule cop_ats_conj_cc_verb{
    pattern{
      V1 -[ats]-> ATS; V2[cat=V];
      coord_rel: ATS -[coord]-> V2}
    commands{del_edge coord_rel; add_edge V1 -[coord]-> V2}}

% Precondition : a conjunct is linked to aother conjunct via a CONJ dependency and the coordination is marked with two correlated adverbs, depending on the first conjunct.
% Action : the two correlated adverbs become the governors of their conjuncts.
  rule correl-cc_c{
    pattern{
      CONJ1[]; CC1[pos=ADV|CC]; cc_rel1: CONJ1 -[cc]-> CC1;
      CC2[pos=ADV|CC]; cc_rel2:CONJ1 -[cc]-> CC2;
      CONJ2[]; conj_rel:CONJ1 -[conj]-> CONJ2}
    without{CC[cat=C,pos=CS]}
    commands{
      del_edge cc_rel1; del_edge cc_rel2; del_edge conj_rel;
      add_edge CC1 -[coord]-> CC2;add_edge CC1 -[dep.coord]-> CONJ1;
      add_edge CC2 -[dep.coord]-> CONJ2}}

  rule coord_wo_left_conjunct {
    pattern {
      CONJ2 -[cc]-> CC; CC[pos=CC]
    }
    without { * -[conj]-> CONJ2 }
    commands {
      del_edge CONJ2 -[cc]-> CC;
      shift_in CONJ2 =[^NE|MWE]=> CC;
      add_edge CC -[dep.coord]-> CONJ2;
    }
  }

  rule post_coord_wo_left_conjunct {
    pattern {
      M -[dep.coord]-> N; N -[punct]-> P; P[lemma="."];
    }
    commands {
      del_edge N -[punct]-> P; add_edge M -[punct]-> P;
    }
  }
}