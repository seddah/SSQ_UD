% =============================================================================================
% Digraph oe
package oe {
  rule oe {
    pattern { N [form=lex.before] }
    commands { N.form=lex.after }
  }
#BEGIN lex
before	after
%-----------------
oeil	œil
oenologie	œnologie
oeuf	œuf
oeufs	œufs
oeuvrant	œuvrant
oeuvre	œuvre
oeuvrer	œuvrer
oeuvres	œuvres
oeuvré	œuvré
Coeur	Cœur
Soeurs	Sœurs
Sacré-Coeur	Sacré-Cœur
Saint-Coeur-de-Marie	Saint-Cœur-de-Marie
Schoelcher	Schœlcher
Voeuil-et-Giget	Vœuil-et-Giget
boeuf	bœuf
belle-soeur	belle-sœur
coeur	cœur
coeurs	cœurs
choeur	chœur
chef-d'oeuvre	chef-d'œuvre
contrecoeur	contrecœur
foehn	fœhn
foetale	fœtale
foetus	fœtus
moeurs	mœurs
main-d'oeuvre	main-d'œuvre
manoeuvrabilité	manœuvrabilité
manoeuvre	manœuvre
manoeuvres	manœuvres
noeud	nœud
noeuds	nœuds
soeur	sœur
soeurs	sœurs
trompe-l'oeil	trompe-l'œil
voeu	vœu
voeux	vœux
#END

}


% =============================================================================================
% Converting every Sequoia category into an UD category.
package cat_ssq_ud{

  rule a{pattern{ A[upos=A]} commands{A.upos=ADJ}}
  rule aux{
    pattern{ V[upos=V]; * -[aux.tps|aux.pass|aux.caus]-> V}
    commands{V.upos=AUX}}
  rule cc{pattern{ C[upos=C,xpos=CC]} commands{C.upos=CCONJ}}
  rule cs{pattern{ C[upos=C,xpos=CS]} commands{C.upos=SCONJ}}
  rule cl{pattern{ CL[upos=CL]} commands{CL.upos=PRON}}
  rule d{pattern{ D[upos=D]} commands{D.upos=DET}}
  rule num{pattern{ D[upos=A|D,s=card]} commands{D.upos=NUM}}
  rule et{pattern{ ET[upos=ET]} commands{ET.upos=X}}
  rule i{pattern{ I[upos=I]} commands{I.upos=INTJ}}
  rule nc{pattern{ N[upos=N,xpos=NC]} commands{N.upos=NOUN}}
  rule npp{pattern{ N[upos=N,xpos=NPP]} commands{N.upos=PROPN}}
  rule p{pattern{ P[upos=P]} commands{P.upos=ADP}}
  rule ponct{pattern{ PONCT[upos=PONCT]} commands{PONCT.upos=PUNCT}}
  rule pro{pattern{ PRO[upos=PRO]} commands{PRO.upos=PRON}}
  rule v{
    pattern{ V[upos=V]}
    without{ * -[aux.tps|aux.pass|aux.caus]-> V}
    commands{V.upos=VERB}}
}

package mwepos {
  rule r {
    pattern{ N[mwepos=lex.before]}
    commands {N.mwepos=lex.after}
  }
#BEGIN lex
before	after
%--------------------------------------------------
NC	NOUN
NPP 	PROPN
V	VERB
P	ADP
P+D	ADP
ADV	ADV
A	ADJ
CC	CCONJ
CS	SCONJ
CLS	PRON
DET	DET
PRO	PRON
I	INTJ
#END
}

% =============================================================================================
% Specific conversion of numerals into the NUM UD category.
package num_ssq_ud {

  rule num{pattern{ D[upos=ADJ|DET,s=card]} commands{D.upos=NUM}}

  rule adj_det_num {
    pattern {NM[upos=ADJ|DET, form=lex.card]}
    without{NM[s=card]}
    commands {NM.upos=NUM}}
#BEGIN lex
card
%----
deux
trois
quatre
cinq
six
sept
huit
dix
#END

  rule other_num {
    pattern { N[upos<>NUM|PRON]; N.lemma = re"[0-9]+\([,.][0-9]+\)?" }
    commands { N.upos = NUM }
  }
}

% =============================================================================================
% Deduction of particular features from the xpos feature.
package pos_feat {
  rule ADJWH{pattern{X[xpos=ADJWH,!s]} commands{X.s=int}}
  rule ADVWH{pattern{X[xpos=ADVWH,!s]} commands{X.s=int}}
  rule CLS{pattern{X[xpos=CLS,!s]}commands{X.s=suj}}
  rule CLO{pattern{X[xpos=CLO,!s]}commands{X.s=obj }}
  rule CLR{pattern{X[xpos=CLR,!s]}commands{X.s=refl}}
  rule DETWH{pattern{DET[xpos=DETWH,!s]}commands{DET.s=int}}
  rule PROREL{pattern{PRO[xpos=PROREL,!s]}commands{PRO.s=rel}}
  rule PROWH{pattern{PRO[xpos=PROWH,!s]}commands{PRO.s=int}}
  rule V{pattern{V[xpos=V,!m]}commands{V.m=ind}}
  rule VIMP{pattern{V[xpos=VIMP,!m]}commands{V.m=imp}}
  rule VINF{pattern{V[xpos=VINF,!m]}commands{V.m=inf}}
  rule VPP{pattern{V[xpos=VPP,!m,!t]}commands{V.m=part;V.t=past}}
  rule VPR{pattern{V[xpos=VPR,!m,!t]}commands{V.m=part;V.t=pst}}
  rule VS{pattern{V[xpos=VS,!m]}commands{V.m=subj}}
}

% =============================================================================================
% Converting every Sequoia feature into an UD feature.
package feat_ssq_ud{

  rule diat_passive{pattern{V[diat=passif]} commands{V.Voice=Pass; del_feat V.diat}}
  rule gender_f{pattern{C[g=f]} commands{C.Gender=Fem; del_feat C.g}}
  rule gender_m{pattern{C[g=m]} commands{C.Gender=Masc; del_feat C.g}}
  rule mood_imp{pattern{C[m=imp]} commands{C.Mood=Imp; C.VerbForm=Fin;del_feat C.m}}
  rule mood_ind{
    pattern{V[m=ind]}
    without{V[t=cond]}
    commands{V.Mood=Ind;V.VerbForm=Fin;del_feat V.m}}
  rule mood_cond{
    pattern{C[m=ind, t=cond]}
    commands{C.Mood=Cnd;C.VerbForm=Fin;C.Tense=Pres;del_feat C.m;del_feat C.t}}
  rule mood_inf{pattern{C[m=inf]} commands{C.VerbForm=Inf; del_feat C.m}}
  rule mood_part{pattern{C[m=part]} commands{C.VerbForm=Part; del_feat C.m}}
  rule mood_pastp{pattern{C[m=pastp]} commands{C.VerbForm=Part; C.Tense=Past; del_feat C.m}}
  rule mood_presp{pattern{C[m=presp]} commands{C.VerbForm=Part; C.Tense=Pres; del_feat C.m}}
  rule mood_subj{pattern{C[m=subj]} commands{C.Mood=Sub;C.VerbForm=Fin; del_feat C.m}}
  rule number_p{pattern{C[n=p]} commands{C.Number=Plur; del_feat C.n}}
  rule number_s{pattern{C[n=s]} commands{C.Number=Sing; del_feat C.n}}
  rule person_1{pattern{C[p=1]} commands{C.Person=1; del_feat C.p}}
  rule person_2{pattern{C[p=2]} commands{C.Person=2; del_feat C.p}}
  rule person_3{pattern{C[p=3]} commands{C.Person=3; del_feat C.p}}
  rule pos_wh{
    pattern{C[xpos=ADVWH|DETWH|PROWH,s=int]}
    commands{C.PronType=Int;del_feat C.xpos;del_feat C.s }}
  rule pos_pro{pattern{C[xpos=PRO]} commands{del_feat C.xpos}}
  rule pos_prorel{
    pattern{C[xpos=PROREL,s=rel]}
    commands{C.PronType=Rel;del_feat C.xpos;del_feat C.s }}
  rule s_card{pattern{C[s=card]} commands{C.NumType=Card;del_feat C.s}}
  rule s_def{pattern{C[upos=DET,s=def]} commands{C.PronType=Art;C.Definite=Def;del_feat C.s}}
  rule s_dem{pattern{C[s=dem]} commands{C.PronType=Dem;del_feat C.s}}
  rule s_ind{pattern{C[upos=DET,s=ind]} commands{C.PronType=Art;C.Definite=Ind;del_feat C.s}}
  rule s_ord{pattern{C[s=ord]} commands{C.NumType=Ord;del_feat C.s}}
  rule s_pers{pattern{C[s=pers]} commands{C.PronType=Prs;del_feat C.s}}
  rule s_poss{pattern{C[s=poss]} commands{C.Poss=Yes;del_feat C.s}}
  rule s_refl{pattern{C[s=refl]} commands{C.Reflex=Yes;del_feat C.s}}
  rule tense_fut{pattern{C[t=fut]} commands{C.Tense=Fut; del_feat C.t}}
  rule tense_impft{pattern{C[t=impft]} commands{C.Tense=Imp; del_feat C.t}}
  rule tense_past{pattern{C[t=past]} commands{C.Tense=Past; del_feat C.t}}
  rule tense_pst{pattern{C[t=pst]} commands{C.Tense=Pres; del_feat C.t}}

}

% =============================================================================================
package promote_mwe {
  rule r  { pattern { N [mwehead=lex.ssq_xpos] } commands {N.init_upos=N.upos; N.upos=lex.upos;  del_feat N.mwehead}}

#BEGIN lex
ssq_xpos	upos
%--------------------------------------------------
NC	NOUN
NPP 	PROPN
P	ADP
P+D	ADP
ADV	ADV
ADJ	ADJ
CC	CCONJ
CS	SCONJ
CLS	PRON
DET	DET
PRO	PRON
#END
}
