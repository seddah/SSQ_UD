# Le système de réécriture SSQtoUD pour passer de la syntaxe de surface de SSQ à la syntaxe de UD

## 1) Correction initiale de l’annotation SSQ

Certains choix effectués par le format SSQ sont discutables. Pour faciliter la conversion dans le format UD, il est nécessaire de les revoir.

C’est tout d’abord le cas des noms propres formés de plusieurs mots.
Lorsqu’un tel nom propre modifie un autre nom, dans SSQ, la dépendance est distribuée sur les composants du nom propre.
Le module `prop_name` fait porter la dépendance sur le composant le plus à gauche, les autres étant rattachés à celui-ci par une dépendance **mod**.

Dans le superlatif, l’article défini peut jouer un double rôle, celui de déterminant et d’expression du superlatif comme dans l’exemple *la plus grande salle*.
Mais il peut ne jouer que le second rôle comme dans *la salle la plus belle*.
Pour ce dernier exemple, dans SSQ, la seconde occurrence de *la* est considérée comme déterminant de *belle*.
Pour éviter une confusion avec le groupe nominal *la belle*, le module `superlative` déplace la source de la dépendance **det** vers l’adverbe exprimant le superlatif, *plus* dans l’exemple.

La segmentation de la phrase en tokens n’est pas la même pour SSQ et pour UD.
Certains mots composés sont formés par ajout d’un préfixe, séparé ou non par un tiret de la racine.
Pour SSQ, ils forment deux tokens, le préfixe de catégorie **PREF** et sa racine reliés par une dépendance **mod**.
Pour UD, ils ne forment qu’un seul token (par exemple *vice-président* dans `annodis.er_00258`).
Le paquet `compound_word` réalise cette transformation.

Dans SSQ, les prépositions amalgamées (*au*, *du*, *duquel*, …) sont représentées sous forme d’un unique token alors que dans UD, elles sont dissociées en deux tokens.
Par exemple, *au* devient *à le*.
Le paquet `phon_unfold` réalise cette dissociation.

## 2) La modification des traits
La valeur du trait **upos**, qui représente la catégorie grammaticale est traduite de SSQ dans UD par le paquet `cat_sq_ud`.
Comme dans UD, les catégories grammaticales sont souvent plus fines que dans SSQ, il faut parfois s’aider du trait **xpos** pour effectuer la traduction.

Ensuite, comme UD utilise une catégorie particulière **NUM** pour les adjectifs et déterminants qui sont des numéraux, le paquet `num_sq_ud` effectue cette génération de la catégorie **NUM**.

Le trait **xpos** va aider le paquet `pos_feat` à générer certains traits permanents ou temporaires, ces derniers étant utiles pour la conversion des dépendances.
Le paquet `feat_sq_ud` effectue la transformation des traits de SSQ en traits de UD.

## 3) La gestion des expressions multi-mots
Dans SSQ, les expressions multi-mots sont représentées sous forme d’une suite de tokens dont la tête est constituée par le token le plus à gauche et dont les composants sont reliés à cette tête par une dépendance **dep_cpd**.
L’expression globale n’a pas forcément la même catégorie grammaticale que sa tête.
Comme il n’y a pas de nœud dans l’arbre d’annotation qui lui est associé, on attache l’information la concernant à des traits spécifiques de la tête. Le trait **mwehead** indique la catégorie grammaticale de l’expression et le trait **mwelemma** son lemme.

UD distingue trois types d’expressions multi-mots :

 * les mots composés dont on connaît la structure syntaxique interne et qui utilisent la dépendance **compound** pour exprimer le rattachement des composants à la tête, [à revoir]
 * les locutions grammaticales totalement figées qui utilisent la dépendance **fixed** pour exprimer le rattachement des composants à la tête,
 * les autres expressions dont on ne cherche pas à exprimer la structure interne et où les composants sont rattachés à celui qui est le plus à gauche par la dépendance **flat**.

Avec le paquet `promote_mwe`, la valeur du trait **upos** de la tête des expressions multi-mots est remplacée par la valeur du trait **mwehead** qui représente la catégorie de l’expression afin que dans le calcul des dépendances UD les expressions multi-mots soient traitées de façon uniforme avec les mots simples [à exploiter avec un exemple].
La catégorie initiale de la tête est conservée dans un trait **initcat**.

## 4) Le traitement des énumérations et des coordinations
SSQ fait la différence entre les énumérations, qui ne comportent aucune conjonction de coordination, des coordinations, qui en comportent une précédant le dernier conjoint.
Dans une énumération, les gouverneurs et les dépendants de l’énumération sont systématiquement distribués sur tous les éléments de l’énumération et il n’y a aucune marque particulière pour indiquer que nous sommes en présence d’une énumération.
Le paquet `enum_ssq_ud` détecte les énumérations SSQ et les transforme en coordinations UD.

Pour les coordinations, SSQ et UD considèrent tous deux que la tête est celle du premier conjoint mais elles diffèrent sur la façon de rattacher les autres conjoints à la tête.
Dans SSQ, la virgule ou la conjonction qui précèdent un conjoint est dépendante de la tête par la relation **coord** et elle gouverne le conjoint qui suit à l’aide de la dépendance **dep.coord**.
UD rattache directement les conjoints suivants à la tête par la dépendance **conj** et la conjonction de coordination qui précède le conjoint est rattaché à ce dernier par une dépendance **CC**.
Si c’est une virgule qui précède le conjoint, elle est rattachée à ce dernier par une dépendance **punct**.
Cette restructuration est réalisée par le paquet `coord_ssq_ud`.

Auparavant, il est nécessaire de traiter les coordinations avec ellipse.
Dans SSQ, les différents constituants du conjoint qui contient une ellipse sont tous rattachés à la conjonction de coordination ou à la virgule qui précède par une dépendance **dep.coord**.
Dans UD, un des constituants est choisi comme tête en fonction d’un ordre d’oblicité sur les fonctions remplies par rapport à l’élément élidé.
Les autres constituants lui sont rattachés par une dépendance **orphan** et il est rattaché à la tête de la coordination par une dépendance **conj**.
C’est le paquet `coord_ellipsis_ssq_ud` qui réalise les dépendances **orphan**.

:warning: [Mettre un exemple complet avec application de `coord_ellipsis_ssq_ud` suivie de `coord_head_ssq_ud`]

## 5) La modification de l’annotation des verbes composés
Pour les verbes composés d’un auxiliaire de temps, du passif ou causatif et d’un verbe plein, il est nécessaire de transférer le trait **VerbForm** de l’auxiliaire vers le verbe plein car ce trait va être utile dans le rattachement ultérieur du verbe plein à ses arguments ou à son gouverneur.
C’est ce qu’effectue le paquet `verb_mood`.
En même temps, il conserve la valeur initiale de **VerbForm** pour le verbe plein dans un nouveau trait **initVerbForm**.
Puis, le paquet `verb_core` change les étiquettes des auxiliaires.

## 6) Les changements de tête
Le paquet `head_chg` change la tête des expressions suivantes :

 * Les syntagmes prépositionnels dans lesquels la préposition gouverne un syntagme nominal.
   La préposition est remplacée par la tête du syntagme nominal comme tête du syntagme prépositionnel et elle devient un dépendant de cette tête marqué **ncase**.
 * Les syntagmes prépositionnels dans lesquels la préposition gouverne un syntagme verbal ou adjectival et les propositions subordonnées.
   La préposition ou la conjonction de subordination est remplacée par la tête du syntagme verbal ou adjectival comme tête du syntagme prépositionnel et elle devient un dépendant de cette tête marqué **mark**.
 * Les adverbes de quantité suivis de la préposition *de* et d’un nom commun ou d’un pronom (*beaucoup de personnes*).
   Dans SSQ, il y a une dépendance **mod** d’un nom vers l’adverbe de quantité et une dépendance det du nom vers la préposition de.
   Dans UD, l’adverbe forme un déterminant complexe avec la préposition *de*.
 * Les expressions étrangères.
   Dans SSQ, les mots composant une expression étrangère sont tous rattachés au mot le plus à droite par des dépendances **dep**.
   Dans UD, ils sont rattachés au mot le plus à gauche par une dépendance **flat:foreign**.

## 7) Le remplacement d’étiquettes de dépendances
Le paquet `label_preddep_sq_ud` remplace les étiquettes de SSQ par les étiquettes de UD pour les dépendances du verbe ou de l’adjectif.
Le paquet `label_noundep_sq_ud` fait de même pour les dépendances du nom.
Le paquet `label_loosejoin_sq_ud` le fait pour les dépendances plus lâches qui expriment d'autres constructions (vocatif, propositions incises, incidentes ou juxtaposées).
Enfin, le paquet `label_transcat_sq_ud` s’applique aux dépendances qui ont comme source différentes catégories.

Étant donné que les étiquettes des dépendances UD dépendent de la catégorie du mot cible, le remplacement des étiquettes de dépendances s’effectue après les changements de têtes des syntagmes prépositionnels et des propositions subordonnées.

## 8) Le traitement de la copule
Dans le format SSQ, la copule gouverne l’attribut du sujet.
Dans le format UD, l’attribut devient la tête et la copule un dépendant de cet attribut marqué **cop**.
Cette transformation est effectuée par le paquet `cop` qui est dissocié du paquet effectuant les autres changements de tête et placé après les paquets remplaçant les étiquettes des dépendances du verbe et de l’adjectif.
Ce traitement postérieur se justifie par des cas comme *c’est l’effervescence parmi les bénévoles*, où il faut d’abord changer l’étiquette de la dépendance de *est* vers *bénévoles* en **advcl** avant de transformer la dépendance **ats** de *est* vers *effervescence* en dépendance inverse **cop**, sinon il ne serait plus possible de distinguer les compléments du nom *effervescence* des compléments de la tête de la phrase.

## 9) La dissociation de certaines expressions multi-mots
Certaines expressions multi-mots sont considérées dans SSQ comme formant un seul token, alors qu’elles sont dissociées dans UD.
Le paquet `spec_split` s’occupe d’expressions spécifiques (négations, euphonies) alors que le paquet `gram_loc_split` traite des locutions grammaticales. Ce traitement se fait plutôt à la fin car pour transformer les dépendances qui portent sur des expressions multi-mots, il est plus facile de considérer ces dernières comme ne formant qu’un seul mot d’une catégorie donnée.

[:warning: donner un exemple].

## 10) Le nettoyage final de l’annotation

 * Le paquet `superlative_restore` rétablit la dépendance **det** des adjectifs au superlatif vers l’article défini utilisé pour exprimer ce superlatif.
 * Le paquet `feat_del` efface certains traits de SSQ qui n’apparaissent plus dans UD et les traits intermédiaires qui ont servi aux calculs.
 * `restore_mwe` est le pendant du paquet `promote_mwe` et mets en place l'annotation finale avec les traits **MWEPOS**.

## 11) Annotations des dates
Application de la nouvelle convention pour les dates (le numéro du jour est la tête).

## 12) Marquage des relations non-converties
Pour différentes raisons, il peut arriver que des relations de SSQ ne soit pas traduites correctement.
Le paquet `fail` ajoute un prefixe `FAIL_` à ces relations pour aider à les repérer facialement.


