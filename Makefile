GREW=grew

self_doc:
	@echo "================================================================================================"
	@echo "Requirement:"
	@echo " - add a (link to) file sequoia.surf.conll with sequoia surface annotation"
	@echo " - add a link to the folder UD_French-Sequoia/"
	@echo " - add a link to the folder UD_tools/"
	@echo ""
	@echo "Targets:"
	@echo " - gui : open the GUI on sequoia.surf.conll"
	@echo " - convert : produce the conversion of seq to the UD annoation (results in _build/ud-***.conllu)"
	@echo " - validate : UD validation of the produced files _build/ud-***.conllu"
	@echo " - diff : Unix diff of the produced files against files in UD_French-Sequoia"
	@echo " - opendiff : Graphical diff of the produced files against files in UD_French-Sequoia"
	@echo " - validate : UD validation of the produced files _build/ud-***.conllu"
	@echo " - install : copy files _build/ud-***.conllu in the folder UD_French-Sequoia"
	@echo "================================================================================================"

gui:
	$(GREW) gui -grs grs/ssq_to_ud/main.grs -i sequoia.surf.conll

convert:
	mkdir -p _build
	@echo "Get Ids from current UD data…"
	grep sent_id UD_French-Sequoia/fr_sequoia-ud-dev.conllu | sed 's/.*=[[:space:]]//g' > _build/dev.ids
	grep sent_id UD_French-Sequoia/fr_sequoia-ud-test.conllu | sed 's/.*=[[:space:]]//g' > _build/test.ids
	grep sent_id UD_French-Sequoia/fr_sequoia-ud-train.conllu | sed 's/.*=[[:space:]]//g' > _build/train.ids
	@echo "Normalisation of the current Sequoia…"
	conll_tool sentid sequoia.surf.conll | sed 's/sentence-text:/text =/' > _build/sequoia.surf.conll
	@echo "Splitting of the current Sequoia…"
	conll_tool split _build/sequoia.surf.conll _build/dev.ids _build/surf-dev.conll
	conll_tool split _build/sequoia.surf.conll _build/test.ids _build/surf-test.conll
	conll_tool split _build/sequoia.surf.conll _build/train.ids _build/surf-train.conll
	@echo "Apply GRS to current Sequoia…"
	$(GREW) transform -grs grs/ssq_to_ud/main.grs -i _build/surf-dev.conll -o _build/ud-dev.conllu
	cat _build/ud-dev.conllu | udapy -s ud.SetSpaceAfterFromText > _build/fr_sequoia-ud-dev.conllu
	$(GREW) transform -grs grs/ssq_to_ud/main.grs -i _build/surf-test.conll -o _build/ud-test.conllu
	cat _build/ud-test.conllu | udapy -s ud.SetSpaceAfterFromText > _build/fr_sequoia-ud-test.conllu
	$(GREW) transform -grs grs/ssq_to_ud/main.grs -i _build/surf-train.conll -o _build/ud-train.conllu
	cat _build/ud-train.conllu | udapy -s ud.SetSpaceAfterFromText > _build/fr_sequoia-ud-train.conllu

validate:
	UD_tools/validate.py --lang fr _build/fr_sequoia-ud-test.conllu
	UD_tools/validate.py --lang fr _build/fr_sequoia-ud-dev.conllu
	UD_tools/validate.py --lang fr _build/fr_sequoia-ud-train.conllu

opendiff:
	opendiff UD_French-Sequoia/fr_sequoia-ud-dev.conllu _build/fr_sequoia-ud-dev.conllu
	opendiff UD_French-Sequoia/fr_sequoia-ud-test.conllu _build/fr_sequoia-ud-test.conllu
	opendiff UD_French-Sequoia/fr_sequoia-ud-train.conllu _build/fr_sequoia-ud-train.conllu

diff:
	diff UD_French-Sequoia/fr_sequoia-ud-dev.conllu _build/fr_sequoia-ud-dev.conllu || true
	diff UD_French-Sequoia/fr_sequoia-ud-test.conllu _build/fr_sequoia-ud-test.conllu || true
	diff UD_French-Sequoia/fr_sequoia-ud-train.conllu _build/fr_sequoia-ud-train.conllu || true

install:
	cp -f _build/fr_sequoia-ud-dev.conllu UD_French-Sequoia/fr_sequoia-ud-dev.conllu
	cp -f _build/fr_sequoia-ud-test.conllu UD_French-Sequoia/fr_sequoia-ud-test.conllu
	cp -f _build/fr_sequoia-ud-train.conllu UD_French-Sequoia/fr_sequoia-ud-train.conllu

# -------------------------------------------------------------------
test:
	head -2365 sequoia.surf.conll > short_SI.conll
	$(GREW) transform -grs grs/ssq_to_ud/main.grs -i short_SI.conll -o short_U.conll
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -i short_U.conll -o short_SF.conll

gui1:
	$(GREW) gui -grs grs/ssq_to_ud/main.grs -i short_SI.conll

gui2:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i short_U.conll

test-diff:
	opendiff short_SI.conll short_SF.conll

# -------------------------------------------------------------------
ud:
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -strat fail -i UD_French-GSD/fr_gsd-ud-dev.conllu -o ud-dev.conll
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -strat fail -i UD_French-GSD/fr_gsd-ud-test.conllu -o ud-test.conll
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -strat fail -i UD_French-GSD/fr_gsd-ud-train.conllu -o ud-train.conll

gui_ud:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i UD_French-GSD/fr_gsd-ud-train.conllu
gui_udd:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i UD_French-GSD/fr_gsd-ud-dev.conllu
gui_udt:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i UD_French-GSD/fr_gsd-ud-test.conllu
