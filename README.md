# Seq2UD

**Seq2UD** is a Graph Rewriting System (GRS) for conversion between two annotation formats used for French data:

 * the **Sequoia** format is used in projects [Deep-sequoia](http://deep-sequoia.inria.fr) and FTB-SPMRL
 * the **UD** format is used in [Universal Dependencies project](http://universaldependencies.org/)

## Usage
To use the GRS, it supposes that the **grew** software is installed (see [Grew webpage](http://grew.fr)).

### Conversion from Sequoia to UD

```shell
grew transform -grs grs/ssq_to_ud/main.grs -i <input_file> -o <output_file>
```

NOTES:

 * If the GRS fails to convert some relation, it changes it with a `FAIL_` prefix

The full procedure to produce UD_French-Sequoia version 2.1 is described in `HOWTO_produce_UD_French-Sequoia-2.1.md`

### Conversion from UD to Sequoia

Two modes are available for this conversion:

  * Robust mode: at the end, UD relations which were not converted are replaced by the `dep` relation to produce a "valid" Sequoia format.

```shell
grew transform -grs grs/ud_to_ssq/main.grs -i <input_file> -o <output_file>
```

  * Safe mode: at the end, UD relations which were not converted are kept and a parallel relation `FAIL` is added (useful for a manual post-edition).

```shell
grew transform -grs grs/ud_to_ssq/main.grs -strat fail -i <input_file> -o <output_file>
```

On 2019/03/11, the conversion (safe mode) of the current version of `UD_French-GSD` produces 0.59% of `FAIL` relations (2,295 for 391,392 tokens).